# Node REST API

This server is a RESTful API With Node.js + MongoDB.

## Running project

You need to have installed Node.js and MongoDB

### Install dependencies

To install dependencies enter project folder and run following command:
```
npm install
```

### Run server

To run server execute:
```
node bin/www
```

Getting your data
```
GET http://SERVER_NAME:PORT/api/nasaApodResult?date=2017-05-22
```

## Author
This is created by Shailesh Mota.

## License

[MIT](https://github.com/ealeksandrov/NodeAPI/blob/master/LICENSE)
