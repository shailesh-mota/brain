var mongoose = require('mongoose');
var Schema = mongoose.Schema;

/*
 * This schema stores the state/snapshot of server that we query and store
 * data from. For example, for NasaApod server since we query by date, we
 * store/update the last successful date for which a record has been fetched
 * and saved in our database.
 */
var ServerStateSnapshot = new Schema({
    nasaapodsnapshot: { // last queried date is updated here
        type: String,
        required: true,
        unique: true
    },
    version: { // when a new field is added in the schema version should be updated
        type: String,
        required: true,
        default: '1'
    }
});

module.exports = mongoose.model('ServerStateSnapshot', ServerStateSnapshot);
