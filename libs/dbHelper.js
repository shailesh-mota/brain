var NasaApodResult = require('./model/NasaApodResult');
var ServerStateSnapshot = require('./model/ServerStateSnapshot');
var log = require('./log')(module);
var libs = process.cwd() + '/libs/';
var config = require(libs + 'config');

//CONFIGURATIONS
var NASA_APOD_SCHEMA_VERSION = config.get('nasaServer:NASA_APOD_SCHEMA_VERSION');
var STATE_SNAPSHOT_SCHEMA_VERSION = config.get('nasaServer:STATE_SNAPSHOT_SCHEMA_VERSION');
var NASA_SERVER = config.get('nasaServer:NASA_SERVER');

function saveNasaApodResponseToDB(response) {
    var result = new NasaApodResult({
        date: response.date,
        explanation: response.explanation,
        title: response.title,
        url: response.url,
        version: NASA_APOD_SCHEMA_VERSION
    });

    result.save(function(err) {
        if (!err) {
            log.info("New result saved to DB", result.id);
            // Update the ServerStateSnapshot Schema
            updateSnapshotSchema(response.date);
        } else {
            log.error("Error while saving to DB", err.name + err.message);
        }
    });
}

function updateSnapshotSchema(lastSavedDate) {

    ServerStateSnapshot.findOne({
        version: STATE_SNAPSHOT_SCHEMA_VERSION
    }, function(err, serverStateSnapshot) {
        if (serverStateSnapshot == null) { // No object exists, first time.
            // create a new and save
            var result = new ServerStateSnapshot({
                nasaapodsnapshot: lastSavedDate,
                version: STATE_SNAPSHOT_SCHEMA_VERSION
            });
            // save it
            result.save(function(err) {
                if (!err) {
                    log.info("Snapshot created with", lastSavedDate);
                } else {
                    log.error("Error while saving to DB", err.name + err.message);
                }
            });
        } else if (!err) { // No error, update the existing snapshot
            serverStateSnapshot.nasaapodsnapshot = lastSavedDate;
            serverStateSnapshot.save();
            log.info("Snapshot updated with", lastSavedDate);
        } else { // error in fetching, log to console
            log.error("Error while fetching snapshot", err.name + err.message);
        }
    });
}

/*
 * Fetches the Snapshot based on server name provided.
 * Caution : can return null values, caller should check for null.
 */
function getSnapshot(serverName, callback) {
    if (serverName == NASA_SERVER) {
        ServerStateSnapshot.findOne({
            version: STATE_SNAPSHOT_SCHEMA_VERSION
        }, function(err, serverStateSnapshot) {
            if (serverStateSnapshot == null) { // No object exists, first time.
                callback(null);
            } else if (!err) { // No error, return the snapshot
                callback(serverStateSnapshot.nasaapodsnapshot); // Last Query Date
            } else { // error in fetching, log to console
                log.error("Error while fetching snapshot", err.name + err.message);
                callback(null);
            }
        });
    }
}

module.exports.saveNasaApodResponseToDB = saveNasaApodResponseToDB;
module.exports.getSnapshot = getSnapshot;
