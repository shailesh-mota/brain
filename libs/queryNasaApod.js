var querystring = require('querystring');
var https = require('https');
var dbHelper = require('./dbHelper.js');
var log = require('./log')(module);
var dateHelper = require('./dateHelper.js');
var libs = process.cwd() + '/libs/';
var config = require(libs + 'config');

//CONFIGURATIONS
var serverName = config.get('nasaServer:NASA_SERVER');
var host = config.get('nasaServer:host');
var apiKey = config.get('nasaServer:apiKey');
var interval = config.get('nasaServer:interval'); // in milliseconds
var endpoint = config.get('nasaServer:endpoint');

// TODO server restarts should bootstrap from last stored date
function bootStrapNasaQuerying() {
    dbHelper.getSnapshot(serverName, function (lastStoredDate) {
    var startDay = 0;
    if (lastStoredDate != null) {
        startDay = dateHelper.getNumberOfDaysFromToday(lastStoredDate) + 1; // SKIP One Day
    }
    // First request send now
    performRequest(dateHelper.getDateInY4M2D2Format(startDay++))
    // Rest send with interval gap
    setInterval(function() {
        performRequest(dateHelper.getDateInY4M2D2Format(startDay++))
    }, interval)});
}

function performRequest(paramDate) {
    var headers = {};
    var date = paramDate;
    var method = 'GET';
    var path = endpoint;

    path += '?' + 'api_key=' + apiKey + '&date=' + date;

    var options = {
        host: host,
        path: path,
        method: method,
        headers: headers
    };

    var req = https.request(options, function(res) {
        res.setEncoding('utf-8');
        log.info("Sending request to NASA server for : ", date);

        var responseString = '';

        res.on('data', function(data) {
            responseString += data;
        });

        res.on('end', function() {
            var responseObject = JSON.parse(responseString);
            log.info("Server Response Received");
            if (isServerResponseValid(responseObject), serverName) {
                dbHelper.saveNasaApodResponseToDB(responseObject);
            }
        });
    });
    //req.write(dataString);
    req.end();
}

/*
 * Returns true if response is valid, false otherwise.
 * Validaity is checked based on all mandatory response params being not null.
 * Check ./model/nasaApodResult.js for response format
 */
// TODO move this to validator.js file or similar
function isServerResponseValid(response, serverType) {
    if (serverType == 'NASA_APOD') {
        return (response.date != null && response.explanation != null &&
            response.title != null && response.url != null);
    }
}

module.exports.bootStrapNasaQuerying = bootStrapNasaQuerying;
