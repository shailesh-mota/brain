var mongoose = require('mongoose');
var libs = process.cwd() + '/libs/';
var log = require(libs + 'log')(module);
var config = require(libs + 'config');
var queryNasaApod = require(libs +'queryNasaApod.js');

mongoose.Promise = global.Promise;
mongoose.connect(config.get('mongoose:uri'));

var db = mongoose.connection;

db.on('error', function (err) {
	log.error('Connection error:', err.message);
});

db.once('open', function callback () {
	log.info("Connected to DB!");
	// After connection established start querying DB
	// Start Querying NASA server
	if(config.get('nasaServer:enabled')) {
			queryNasaApod.bootStrapNasaQuerying();
	}
});

module.exports = mongoose;
