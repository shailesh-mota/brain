var express = require('express');
var router = express.Router();
var libs = process.cwd() + '/libs/';
var log = require(libs + '/log')(module);
var db = require(libs + 'db/mongoose');
var NasaApodResult = require(libs + 'model/NasaApodResult');

// return 20 NasaApodResult
//TODO SAME QUERY : db.nasaapodresults.insert({copyright: 'asd',date: 'unique',explanation: 'asd',hdurl: 'asd',media_type: 'asd',service_version: 'asd',title: 'asd',url: 'asd'});
router.get('/', function(req, res) {
    //console.log(req.query.date);
    log.info("Client query : " + req.query.date);

    // TODO validation that date is in proper format

    // if no date then return first 50 results
    // else query the DB with particular date

    NasaApodResult.find({date:req.query.date}, function(err, nasaApodResult) {

        if (!nasaApodResult) {
            res.statusCode = 404;

            return res.json({
                error: 'Not found'
            });
        }

        if (!err) {
            return res.json({
                status: 'OK',
                nasaApodResult: nasaApodResult
            });
        } else {
            res.statusCode = 500;
            log.error('Internal error(%d): %s', res.statusCode, err.message);

            return res.json({
                error: 'Server error'
            });
        }
    });
});

module.exports = router;
