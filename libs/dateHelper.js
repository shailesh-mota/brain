var LocalDate = require('js-joda').LocalDate;
var ChronoUnit = require('js-joda').ChronoUnit;

/*
 * Returns date in ISO8601 formatted text string
 */
function getDateInY4M2D2Format(minusDays) {
    var dt = LocalDate.now();
    return dt.minusDays(minusDays);
}

/*
 * Returns the number of days between today and the
 * date passed as ISO8601 formatted string.
 */
function getNumberOfDaysFromToday(date) {
    var today = LocalDate.now();
    var pastDate = LocalDate.parse(date);
    return pastDate.until(today, ChronoUnit.DAYS);
}

module.exports.getDateInY4M2D2Format = getDateInY4M2D2Format;
module.exports.getNumberOfDaysFromToday = getNumberOfDaysFromToday;
